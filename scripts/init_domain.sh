#!/bin/bash
# Copy config samples and replace localhost in config samples with
# given domain name

if [ -z "$1" ]; then
  cat <<- EOF
	Usage: $0 domain_name

	domain_name	exple: example.com
EOF
  exit 1
fi

DOMAIN_NAME="$1"
LDAP_DN=$( echo 'dc='$DOMAIN_NAME | sed 's/\./,dc=/g')
CONFIG_PATH=config
SAMPLE_PATH=$CONFIG_PATH/samples

sed 's/ localhost/ '$DOMAIN_NAME'/' $SAMPLE_PATH/init.ldif | \
	sed 's/dc=localhost/'$LDAP_DN'/' > $CONFIG_PATH/init.ldif
sed 's/=localhost/='$DOMAIN_NAME'/' $SAMPLE_PATH/ldap.env > $CONFIG_PATH/ldap.env
sed 's/=localhost/='$DOMAIN_NAME'/' $SAMPLE_PATH/sip.env > $CONFIG_PATH/sip.env
sed 's/=localhost/=pla.'$DOMAIN_NAME'/' $SAMPLE_PATH/phpldapadmin.env > $CONFIG_PATH/phpldapadmin.env

echo "Domain name set in SIP and LDAP env/database"
