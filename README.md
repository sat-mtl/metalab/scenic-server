# sat-metalab/scenic-server

[Scénic](https://github.com/sat-metalab/SCENIC/wiki) is a software platform compatible with various data streams and formats: video, audio, OSC controls, MIDI, data... These data streams are several sources of data that can be reformatted and then exchanged in real time with one or several distant Scénic stations.

This is the SIP+STUN+TURN server used by the development team, packaged in Docker containers. It also includes OpenLDAP server and phpLDAPadmin for user authentication and management.

## Requirements

* Python >=3.0
* [Docker](https://github.com/docker/docker) (make sure your user is part of *docker* group)
* [Docker Compose](https://github.com/docker/compose)

It is highly recommended to install on a machine accessible with your own public domain name. Ports 80, 443, 3478 (STUN) and 5060(SIP) must be allowed by the eventual firewall.

## Quick Start

### Install Docker Compose

```shell
curl -L "https://github.com/docker/compose/releases/download/1.10.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
```

### Clone this repository:

```shell
git clone https://github.com/sat-metalab/scenic-server.git
```

### Configure your environment

Set the top domain name:

```shell
scripts/init_domain.sh example.com
```

In docker-compose.yml, you may also want to set a different location for data volumes by replacing all *./volumes* with */srv/scenic-server*, or any other directory.

### Start all the dockerized services

```shell
docker-compose up -d
```

### Initialize the LDAP database with a minimalistic sample

```shell
scripts/restore_ldif.sh config/init.ldif
```

## LDAP User Management

To manage users, you can use *ldapmodify* or *ldapvi* inside the *openldap* container. Or log in phpLDAPadmin in your browser, at the URL defined by VIRTUAL_HOST in config/phpmyadmin.env (http://pla.<your-domain>). The database administrator is *cn=admin,dc=example,dc=com* and the password is the one defined by the LDAP_PASSWORD environment variable.

It is highly recommended that you initialize your LDAP database with this structure (from *config/init.ldif*).

Users with the *inetOrgPerson* object class will be allowed to log in phpLDAPadmin using their *uid* and *userPassword*. Those who are in the *cn=Administrator* group will be allowed to manage the database.

Users with the *SIPIdentity* class object will be allowed to log in the SIP server using their *uid* and *SIPIdentityPassword*.

Coturn server embeds a cronjob which syncs its database with LDAP every 5 min.

## Helper Scripts

Three helper scripts are included in *./scripts/*:

- **init_domain.sh** : Change default domain name (localhost) and SIP and LDAP env.
- **restore_ldif.sh** : Restore a minimalistic LDAP database
- **update_coturn_users.sh** : Import all LDAP users in the STUN/TURN server database (no automatic import at this time)
